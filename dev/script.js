// 2024 Arnaud Champollion, licence GNU/GPL 3.0

// Constituants de la page
const conteneur = document.getElementById('conteneur');
const divOnglets = document.getElementById('zone-onglets');
const graphiqueNiveaux = document.getElementById('graphique-niveaux');
const valeurEffectifTotal = document.getElementById('indicateur-effectif-total');
const darkbox = document.getElementById('darkbox');
const divAide = document.getElementById('aide');
const divApropos = document.getElementById('a-propos');
const divOptions = document.getElementById('options');
const divAjoutNiveaux = document.getElementById('ajout-niveaux');
const valeurMoyenne = document.getElementById('indicateur-moyenne');
const inputNbClasses = document.getElementById('input-nb-classes');
const divClasses = document.getElementById('classes');
const divStats = document.getElementById('stats-niveaux');
const inputNbMin = document.getElementById('input-nb-min');
const caseSauter = document.getElementById('case-sauter');
const inputsEffectifs = document.querySelectorAll('.input-effectif');
const caseInf = document.getElementById('case-inf');
const caseTripleNiveaux = document.getElementById('case-triple');
const boutonsAjoutNiveau = divAjoutNiveaux.querySelectorAll('.ajout');
const boutonCsv = document.getElementById('bouton-csv');
const boutonReplier = document.getElementById('bouton-replier');
const boutonAuto = document.getElementById('bouton-auto');
const boutonDeplacer = document.getElementById('bouton-deplacer');
const divEffectifs = document.getElementById('effectifs');
const casesGrouper = document.querySelectorAll("input[id*='case-grouper']");
const casesIsoler = document.querySelectorAll("input[id*='case-isoler']");
const inputsLimite = document.querySelectorAll("input[id*='input-limite']");
const classesEnLignesRadio = document.getElementById('classes-en-lignes');
const classesEnColonnesRadio = document.getElementById('classes-en-colonnes');
const spanOnglets = document.querySelectorAll('.onglet');
const body = document.body;


function init(){
    // Variables globales
    ongletEnCours = null;
    compteurOnglets = 1;
    menuOuvert=false;
    nombreOnglets=0;
    texteOngletEnCours=null;
    prioriteInf = false;
    OngletAMettreEnCours = null;


    // Liste des niveaux
    listeNiveaux=['tps','ps','ms','gs','cp','ce1','ce2','cm1','cm2'];
    // Tableau des onglets
    listeOnglets=[];
    // Données des onglets
    sauvegardesOnglets = {};
}

init();


// Onglet vierge
modeleSauvegarde = {
    auto: true,
    caseSauter: false,
    caseTripleNiveaux: false,
    minGroupe: 4,
    modif: false,
    nombreDeClasses: 2,
    niveaux: {
        tps: {
            effectif: 0,
            max: 30,
            couleur: "rgb(255, 255, 0)",
            grouper: false,
            isoler: false
        },
        ps: {
            effectif: 0,
            max: 30,
            couleur: "rgb(204, 255, 51)",
            grouper: false,
            isoler: false
        },
        ms: {
            effectif: 0,
            max: 30,
            couleur: "rgb(102, 255, 51)",
            grouper: false,
            isoler: false
        },
        gs: {
            effectif: 0,
            max: 24,
            couleur: "rgb(0, 204, 102)",
            grouper: false,
            isoler: false
        },
        cp: {
            effectif: 0,
            max: 24,
            couleur: "rgb(0, 255, 204)",
            grouper: false,
            isoler: false
        },
        ce1: {
            effectif: 0,
            max: 24,
            couleur: "rgb(0, 204, 255)",
            grouper: false,
            isoler: false
        },
        ce2: {
            effectif: 0,
            max: 30,
            couleur: "rgb(0, 102, 255)",
            grouper: false,
            isoler: false
        },
        cm1: {
            effectif: 0,
            max: 30,
            couleur: "rgb(204, 102, 255)",
            grouper: false,
            isoler: false
        },
        cm2: {
            effectif: 0,
            max: 30,
            couleur: "rgb(255, 0, 102)",
            grouper: false,
            isoler: false
        }
    },    
    repartition: {} 
}

function valeursDepart(){

    //Décocher toutes les cases à cocher et déclencher les actions en cas de clic
    casesGrouper.forEach(function(input) {
        input.checked = false;
        input.disabled = false;
        input.addEventListener("change", changeGrouper);
    });

    casesIsoler.forEach(function(input) {
        input.checked = false;
        input.disabled = false;
        input.addEventListener("change", changeIsoler);
    });

    classesEnLignesRadio.checked=true;
    classesEnColonnesRadio.checked=false;

    modif=false;
    minGroupe = 4;
    nombreDeClasses = 2;
    effectifTotal = 0;
    auto=true;

    if (auto){boutonAuto.classList.add('actif');boutonDeplacer.classList.remove('actif');}
    else {boutonDeplacer.classList.add('actif');boutonAuto.classList.remove('actif');}

    caseSauter.checked=false;
    caseTripleNiveaux.checked=true;
    //caseAuto.checked=true;
    repartition={};

    // Liste détaillée
    niveaux = {
        tps: {
            effectif: 0,
            max: 30,
            couleur: "rgb(255, 255, 0)",
            grouper: false,
            isoler: false
        },
        ps: {
            effectif: 0,
            max: 30,
            couleur: "rgb(204, 255, 51)",
            grouper: false,
            isoler: false
        },
        ms: {
            effectif: 0,
            max: 30,
            couleur: "rgb(102, 255, 51)",
            grouper: false,
            isoler: false
        },
        gs: {
            effectif: 0,
            max: 24,
            couleur: "rgb(0, 204, 102)",
            grouper: false,
            isoler: false
        },
        cp: {
            effectif: 0,
            max: 24,
            couleur: "rgb(0, 255, 204)",
            grouper: false,
            isoler: false
        },
        ce1: {
            effectif: 0,
            max: 24,
            couleur: "rgb(0, 204, 255)",
            grouper: false,
            isoler: false
        },
        ce2: {
            effectif: 0,
            max: 30,
            couleur: "rgb(0, 102, 255)",
            grouper: false,
            isoler: false
        },
        cm1: {
            effectif: 0,
            max: 30,
            couleur: "rgb(204, 102, 255)",
            grouper: false,
            isoler: false
        },
        cm2: {
            effectif: 0,
            max: 30,
            couleur: "rgb(255, 0, 102)",
            grouper: false,
            isoler: false
        }
    };

    inputNbMin.value=minGroupe;
    caseInf.checked = prioriteInf;
    inputNbClasses.value=nombreDeClasses;

    for (let niveau in niveaux) {    
        document.getElementById('input-' + niveau).style.backgroundColor = niveaux[niveau].couleur;
        document.getElementById('input-limite-'+niveau).value = niveaux[niveau].max;
        document.getElementById('input-limite-'+niveau).style.backgroundColor = niveaux[niveau].couleur;
    }
}

valeursDepart();

function lireStockageLocal(){

    if (localStorage.getItem('repartition-classeur')){

        let TousLesSpanOnglets = divOnglets.querySelectorAll('.onglet');
        TousLesSpanOnglets.forEach(onglet => {
            onglet.remove();
        });

        sauvegardesOnglets = litObjet('classeur');
        texteOngletEnCours = litString('onglet-en-cours');
        
        // Parcourir chaque clé dans sauvegardesOnglets
        for (let cle in sauvegardesOnglets) {
            // Appeler nouvelOnglet() pour chaque objet sauvegardé
            nouvelOnglet("restaurer", cle);
        }

        if (OngletAMettreEnCours){
            changeOnglet(OngletAMettreEnCours);
        }

        

    }

    else if (localStorage.getItem('repartition-repartition')){
        nouvelOnglet('compatibilite');
        console.log("Présence de sauvegarde automatique dans le cache du navigateur");
        niveaux = litObjet('niveaux');
        repartition = litObjet('repartition');
        nombreDeClasses = litInt('nombreDeClasses');
        minGroupe = litInt('minGroupe');
        modif = litBool('modif');
        auto = litBool('auto');
        caseSauter.checked = litBool('caseSauter.checked');
        caseTripleNiveaux.checked = litBool('caseTripleNiveaux.checked');
        majAffichages();

    } else {
        nouvelOnglet('nouveau');  
    }
   

}

function majAffichages(){

    console.log('majAffichages');
    console.log(niveaux);


    inputNbMin.value=minGroupe;
    caseInf.checked = prioriteInf;
    inputNbClasses.value=nombreDeClasses;

    let somme=0;

    for (niveau in niveaux){
        let effectif=niveaux[niveau].effectif;
        somme+=effectif;
        let grouper=niveaux[niveau].grouper;
        let isoler=niveaux[niveau].isoler;
        document.getElementById('input-'+niveau).value=effectif;
        document.getElementById('case-grouper-'+niveau).checked=grouper;
        document.getElementById('case-isoler-'+niveau).checked=isoler;
        if (niveaux[niveau].max){
            document.getElementById('input-limite-'+niveau).value = niveaux[niveau].max;
        }
    }

    moyenneParClasse= somme / nombreDeClasses;
    effectifTotal = somme;
    valeurEffectifTotal.innerText = somme;
    valeurMoyenne.innerText = moyenneParClasse.toFixed(1).replace(".", ",");

    if (auto){
        traiter();
    } else {        
        creeClasses('manuel');
        autoDeplacer(false,'depuisStockage');        
    }
}

function lireStockageLocalPermanent() {
    return new Promise((resolve, reject) => {
        if (localStorage.getItem('permanent-repartition-disposition') === 'colonnes') {        
            classesEnLignesRadio.checked = false;
            classesEnColonnesRadio.checked = true;
        } else {
            classesEnLignesRadio.checked = true;
            classesEnColonnesRadio.checked = false;
        }
        resolve();
    });
}

lireStockageLocalPermanent().then(() => {
    mettreAJourDisposition();
    lireStockageLocal();
});

function mettreAJourDisposition() {
    console.log('classesEnLignesRadio.checked '+classesEnLignesRadio.checked);
    console.log('classesEnColonnesRadio.checked '+classesEnColonnesRadio.checked);

    if (classesEnLignesRadio.checked) {
        console.log('lignes');
        conteneur.classList.remove('inverse');
        localStorage.setItem('permanent-repartition-disposition','lignes');
    } else if (classesEnColonnesRadio.checked) {
        conteneur.classList.add('inverse');
        localStorage.setItem('permanent-repartition-disposition','colonnes');
    }

}

// Action des cases "grouper"
function changeGrouper(event){
    let isChecked = event.target.checked;
    let checkboxId = event.target.id;
    let idSuffix = checkboxId.substring("case-grouper-".length);
    if (niveaux.hasOwnProperty(idSuffix)) {
        niveaux[idSuffix].grouper = isChecked;
    }
    modifValeur();
}

// Action des cases "isoler"
function changeIsoler(event){
    let isChecked = event.target.checked;
    let checkboxId = event.target.id;
    let idSuffix = checkboxId.substring("case-isoler-".length);
    if (niveaux.hasOwnProperty(idSuffix)) {
        if (isChecked){niveaux[idSuffix].isoler = 1;}
        else {niveaux[idSuffix].isoler = false;}
    }
    limiterIsoler();
    modifValeur();
}

// Empêcher d'isoler trop de classes
function limiterIsoler(){

    console.log('nombreDeNiveauxAisoler '+nombreDeNiveauxAisoler);
    console.log('nombreDeClasses '+nombreDeClasses);


    if (nombreDeNiveauxAisoler === nombreDeClasses - 1){
        casesIsoler.forEach(function(input) {
            if (!input.checked){
                input.disabled=true;
            }    
        });
    } else {
        casesIsoler.forEach(function(input) {
                input.disabled=false;
        });
    }

}

// Action lancée à chaque modification du formulaire
function traiter() {  
    console.log('TRAITEMENT DU FORMULAIRE')  
    let boutonsDeplacer=document.querySelectorAll('.modif');
    boutonsDeplacer.forEach(bouton => {
        bouton.classList.add('hide');
    });
    prioriteInf = caseInf.checked;
    // Récupération des données
    nombreDeClasses = inputNbClasses.value; // Nombre de classes    
    minGroupe = parseInt(inputNbMin.value); // Nombre d'élèves minimum dans un groupe
    effectifTotal = 0;
    nombreElevesHorsMoyenne = 0;

    grouperEffectifsNiveaux = []; // Liste des niveaux groupés
    isolerEffectifsNiveaux = []; // Liste des niveaux isolés
    restesEffectifsNiveaux = []; // Liste temporaire d'effectifs par niveau
    
    nombredeNiveauxNonVides=0;


    for (let niveau in niveaux) { // Pour chaque niveau
        console.log('Formulaire - traitement du niveau '+niveaux[niveau]);
        // Récupération de l'effectif
        let effectifInput = parseInt(document.getElementById('input-' + niveau).value);
        let max = parseInt(document.getElementById('input-limite-' + niveau).value);
        console.log('Effactif '+effectifInput)
        if (isNaN(effectifInput)) { // Si donnée non numérique, effectif = 0
            effectifInput = 0;
        }

        if (isNaN(max)) { // Si donnée non numérique, effectif = 0
            max = 30;
        }

        niveaux[niveau].effectif = effectifInput; // Mise à jour des données
        niveaux[niveau].max = max; // Mise à jour des données
        effectifTotal += niveaux[niveau].effectif; // Ajout à l'effectif total   
        // Copie da valeur "grouper" du niveau en cours vers la liste 
        grouperEffectifNiveau = niveaux[niveau].grouper;
        grouperEffectifsNiveaux.push(grouperEffectifNiveau);
        // Copie da valeur "isoler" du niveau en cours vers la liste 
        isolerEffectifNiveaux = niveaux[niveau].isoler;
        isolerEffectifsNiveaux.push(isolerEffectifNiveaux);

        if (effectifInput != 0){
            nombredeNiveauxNonVides+=1;
        }

        if (isolerEffectifNiveaux) { 
            nombreElevesHorsMoyenne+=effectifInput; // Ajout des élèves du niveau dans l'effectif hors moyenne
        }
    }        

    /* Calcul automatique du nombre de classes
    if (caseAuto.checked){
        inputNbClasses.disabled=true;
        let nombreIdeal;
        if (effectifTotal === 0) {
            nombreIdeal= 0;
        }    
        else if (effectifTotal <= 15) {
            nombreIdeal= 1;
        } else if (effectifTotal <= 36) {
            nombreIdeal = 2;
        } else {
            nombreIdeal = Math.floor(1+(effectifTotal / 24));
        }
        inputNbClasses.value = nombreDeClasses = nombreIdeal;
    } else {
        inputNbClasses.disabled=false;
    }
    */
    
    // Calcul du nombre de niveaux isolés
    nombreDeNiveauxAisoler = isolerEffectifsNiveaux.filter(Boolean).length;

    // Calcul des statistiques
    if (nombreDeClasses!=0){
        moyenneParClasse = effectifTotal / nombreDeClasses; // Moyenne
        resteAdistribuer = effectifTotal % nombreDeClasses; // Reste
    } else {
        moyenneParClasse = 0;
        resteAdistribuer = 0;
    }

    moyenneParClasseEntiere = Math.floor(moyenneParClasse); // Moyenne entière
        
    // Mise à jour de l'affichage
    valeurEffectifTotal.innerText = effectifTotal;
    valeurMoyenne.innerText = moyenneParClasse.toFixed(1).replace(".", ",");
    

    for (let niveau in niveaux) { // Pour chaque niveau
        // On vérifie si le niveau est groupé et/ou isolé
        grouperEffectifNiveau = niveaux[niveau].grouper;
        isolerEffectifNiveaux = niveaux[niveau].isoler;

        // On récupère son effectif déclaré
        let effectifInput = parseInt(document.getElementById('input-' + niveau).value);
        if (isNaN(effectifInput)) {
            effectifInput = 0;
        }
        
        // Si l'effectif est supérieur à la moyenne * 1.2 , on empêche le groupement
        if (effectifInput > moyenneParClasse * 1.2) {
            niveaux[niveau].grouper=false;
            document.getElementById('case-grouper-' + niveau).checked=false;
            document.getElementById('case-grouper-' + niveau).disabled=true;
            if (grouperEffectifNiveau && effectifInput > moyenneParClasse){
                niveaux[niveau].grouper=false;
                document.getElementById('case-grouper-' + niveau).checked=false;
            }
        } else {
            document.getElementById('case-grouper-' + niveau).disabled=false;
        }
       
        // Si le niveau est isolé et qu'il est > à la limite, on le scinde.
        let nombreDeParties
        if (isolerEffectifNiveaux && effectifInput > niveaux[niveau].max){
            nombreDeParties = Math.ceil(effectifInput / niveaux[niveau].max);
            niveaux[niveau].isoler=nombreDeParties;
        } else if (isolerEffectifNiveaux && effectifInput > moyenneParClasse * 1.2){
            nombreDeParties = Math.ceil(effectifInput / (moyenneParClasse * 1.2));
            niveaux[niveau].isoler=nombreDeParties;
        }
        
        if (isolerEffectifNiveaux){
            nombreDeNiveauxAisoler+=(niveaux[niveau].isoler - 1);
            console.log(niveau+' isolé et réparti en '+niveaux[niveau].isoler+' classes');            
        }
    }

    // On recalcule la moyenne en ne tenant pas compte des niveaux isolés.
    nombreElevesNonIsoles = effectifTotal-nombreElevesHorsMoyenne;
    nombreDeClassesNonIsolees = nombreDeClasses - nombreDeNiveauxAisoler;
    moyenneEffectifHorsIsoler = nombreElevesNonIsoles / nombreDeClassesNonIsolees;

    console.log('moyenneEffectifHorsIsoler '+moyenneEffectifHorsIsoler)

    /*
    if (caseAuto.checked){ // Prise en compte des niveaux isolés et des limitations
        let correctifNombreDeclasses = 0;
        for (niveau in niveaux){
            let diff;
            if (niveaux[niveau].max < 24){
                diff =  (niveaux[niveau].effectif / niveaux[niveau].max)- (niveaux[niveau].effectif / 24);
                console.log('diff '+diff);
            } else {
                diff = 0;
            }
            if (diff>0){correctifNombreDeclasses+=(Math.floor(diff)+1);}
        }

        inputNbClasses.value = nombreDeClasses = nombreDeClasses+correctifNombreDeclasses;
   

        // On recalcule tout
        moyenneParClasse = effectifTotal / nombreDeClasses; // Moyenne
        resteAdistribuer = effectifTotal % nombreDeClasses; // Reste
        moyenneParClasseEntiere = Math.floor (moyenneParClasse); // Moyenne entière
        nombreElevesNonIsoles = effectifTotal-nombreElevesHorsMoyenne;
        nombreDeClassesNonIsolees = nombreDeClasses - nombreDeNiveauxAisoler;
        moyenneEffectifHorsIsoler = nombreElevesNonIsoles / nombreDeClassesNonIsolees;
        valeurMoyenne.innerText = moyenneParClasse.toFixed(1).replace(".", ",");

        console.log('Moyenne hors effectifs isolés '+moyenneEffectifHorsIsoler)
    }
*/
    //divStats.classList.remove('hide'); // On affiche le panneau des statistiques

    limiterIsoler(); // On limite le nombre de niveaux isolables
   //majGraphique(); // On met à jour le graphique
    if (effectifTotal != 0){
        creeClasses('auto'); // On lance la création des classes
    } else {
        sauveObjet('classeur',sauvegardesOnglets);
        //sauveObjet('niveaux',niveaux);
        //sauveObjet('repartition',repartition);
        //sauveInt('nombreDeClasses',nombreDeClasses);
        //sauveInt('minGroupe',minGroupe);
        //sauveBool('modif',modif);
        //sauveBool('auto',auto);
        //sauveBool('caseSauter.checked',caseSauter.checked);
        //sauveBool('caseAuto.checked',caseAuto.checked);
        //sauveBool('caseTripleNiveaux.checked',caseTripleNiveaux.checked);
    }
}

/*
function majGraphique() {
    for (let niveau in niveaux) {
        let effectif = niveaux[niveau].effectif;
        let largeur = 100* effectif / effectifTotal;
        let element = document.getElementById('graphique-'+niveau);
        element.style.width=largeur+'%';
        element.style.backgroundColor=niveaux[niveau].couleur;
    }
}
*/


function creeClasses(mode) {

    console.log('Création des classes en mode '+mode)

    divClasses.innerHTML=''; // On vide la div des classes

    if (mode!='manuel'){
        repartition={}; // On vide l'ojbjet répartition
    }

    for (let i = 0; i < nombreDeClasses; i++) { // Boucle effectuée autant de fois que le nombre de classes

        // Création des éléments qui vont contenir les données
        let nouvelleClasse = document.createElement('div');
        nouvelleClasse.classList.add('classe');
        divClasses.append(nouvelleClasse);

        let nouvelleZone = document.createElement('div');
        nouvelleZone.classList.add('zone-valeurs','intitule','fond-gris');



        let nouvelleEtiquette = document.createElement('div');
        nouvelleEtiquette.classList.add('etiquette-classe');
        nouvelleZone.append(nouvelleEtiquette);



        let nouvelleValeur = document.createElement('div');
        nouvelleValeur.classList.add('valeur-classe');
        nouvelleZone.append(nouvelleValeur);

        nouvelleClasse.append(nouvelleZone);

        let classe = 'classe_' + i;

        if (mode!='manuel'){
            repartition[classe] = {};
            repartition[classe].nom = 'classe ' + (i+1);
            // Nombre d'élèves de TPS PS MS CP ... dans cette classe, au début on met tout à zéro.        
            repartition[classe].effectifsParNiveaux = [0,0,0,0,0,0,0,0,0,];
            repartition[classe].nombreDeNiveaux = 0;
        }

        nouvelleEtiquette.innerText = repartition[classe].nom;
        let n=0;
        for (let niveau in niveaux) { // pour chaque niveau
            let indice = n;
            // Création des blocs de niveaux TPS PS MS ...
            let nouvelleZone = document.createElement('div');            
            nouvelleZone.classList.add('zone-valeurs','detail');
            nouvelleZone.style.backgroundColor=niveaux[niveau].couleur;
            let nouvelleEtiquette = document.createElement('div');
            nouvelleEtiquette.classList.add('etiquette-zone-valeurs');
            nouvelleEtiquette.innerText= niveau.toUpperCase();

  

            let nouvelleValeur = document.createElement('div');
            nouvelleValeur.classList.add('valeur-zone-valeurs');
            nouvelleValeur.innerText= '0';
            let nouveauBoutonBas = document.createElement('button');
            nouveauBoutonBas.classList.add('modif','bas','hide');
            nouveauBoutonBas.addEventListener('click', function() {
                deplace(i, indice, 1);
            });
            let nouveauBoutonHaut = document.createElement('button');
            nouveauBoutonHaut.classList.add('modif','haut','hide');
            nouveauBoutonHaut.addEventListener('click', function() {
                deplace(i, indice, -1);
            });

            let nouveauBoutonMoins = document.createElement('button');
            nouveauBoutonMoins.classList.add('plusmoins','moins','hide');
            nouveauBoutonMoins.addEventListener('click', function() {
                plusmoins(i, indice, -1);
            });

            let nouveauBoutonPlus = document.createElement('button');
            nouveauBoutonPlus.classList.add('plusmoins','plus','hide');
            nouveauBoutonPlus.addEventListener('click', function() {
                plusmoins(i, indice, 1);
            });


            if (i+1<nombreDeClasses){nouvelleZone.appendChild(nouveauBoutonBas);}
            if (i>0){nouvelleZone.appendChild(nouveauBoutonHaut);}
            nouvelleZone.appendChild(nouveauBoutonMoins);
            nouvelleZone.appendChild(nouveauBoutonPlus);
            nouvelleZone.appendChild(nouvelleEtiquette);
            nouvelleZone.appendChild(nouvelleValeur);
            nouvelleClasse.appendChild(nouvelleZone);
            n+=1;
        }

        let nouveauBouton = document.createElement('button');
        nouveauBouton.classList.add('bouton-plus-classe','hide');
        nouveauBouton.innerText='+...'
        nouvelleClasse.append(nouveauBouton);
        nouveauBouton.addEventListener('click', function() {
            ajouteNiveau(i);
        });


    }
    tour=0;
    nombreDelevesAcompterEnMoins=correctifEleves=0;
    classesAcompterEnMoins=correctifClasses=0;
    repartitionClasses(mode); // Lancement de la répartition des niveaux dans les classes
    
}

function verifieNomExiste(nom) {
    // Parcourir les clés de l'objet sauvegardesOnglets
    for (let key in sauvegardesOnglets) {
        // Comparer chaque clé avec l'origine
        if (key === nom) {
            return true; // Si une clé correspond à l'origine, retourne true
        }
    }
    return false; // Si aucune correspondance n'est trouvée, retourne false
}

function scrollOnglets(sens){
    divOnglets.scrollLeft += sens*50;
}

function nouvelOnglet(mode,origine) {
    if (ongletEnCours){
        ongletEnCours.classList.remove('encours');
    }

    let nouvelleSpanOnglet = document.createElement('span');
    ongletEnCours = nouvelleSpanOnglet;
    listeOnglets.push(nouvelleSpanOnglet);
    
    if (mode==='dupliquer'){
        let nomOrigine = nouveauNom = origine.innerHTML;
        let numero = 2;
        while (verifieNomExiste(nouveauNom)){
            nouveauNom = nomOrigine + '.' + numero;
            numero+=1;            
        }
        nouvelleSpanOnglet.innerHTML = nouveauNom;

    } else if (mode==='restaurer'){
        nouvelleSpanOnglet.innerHTML = origine;
        if (origine===texteOngletEnCours){
            OngletAMettreEnCours=nouvelleSpanOnglet;
        }
    } 
    
    else {
        nouvelleSpanOnglet.innerHTML = 'Répartition '+compteurOnglets;
    }
    compteurOnglets+=1;
    nombreOnglets+=1;
    nouvelleSpanOnglet.classList.add('onglet','encours');

    // ÉVÉNEMENTS SUR L'ONGLET //

    // Clic sur l'onglet --> devient actif
    nouvelleSpanOnglet.addEventListener('click', function() {
        changeOnglet(nouvelleSpanOnglet);
    });

    // Double clic sur l'onglet --> renommer
    nouvelleSpanOnglet.addEventListener('dblclick', function() {
        renommeOnglet(nouvelleSpanOnglet);
    });

    // Clic droit sur l'onglet --> menu contextuel
    nouvelleSpanOnglet.addEventListener('contextmenu', function(e) {
        e.preventDefault();
        menuContextuel(e,nouvelleSpanOnglet);
    });

    // Empêche l'utilisateur de coller du contenu dans l'onglet
    nouvelleSpanOnglet.addEventListener('paste', function(event) {
        event.preventDefault(); // Empêche le comportement par défaut de collage
    });



    // Touche entrée : on valide
    nouvelleSpanOnglet.addEventListener('keydown', function(event) {
        if (event.keyCode === 13) { // Vérifie si la touche appuyée est Entrée
            nouvelleSpanOnglet.blur(); // Déclenche l'événement blur
        }
});

// Perte de focus : on valide
    nouvelleSpanOnglet.addEventListener('blur', function() {
        console.log('perte de focus');
        nouvelleSpanOnglet.contentEditable = false;
        verifieNouveauNom(nouvelleSpanOnglet);
});

    divOnglets.appendChild(nouvelleSpanOnglet);

    if (mode==='nouveau'){
        divClasses.innerHTML='';          
        restaureObjet('nouveau');
    }

    if (mode==='restaurer'){  
        restaureObjet();
    } else {
        sauveString('onglet-en-cours',ongletEnCours.innerText);
    }

    if (mode==='compatibilite'){  
        mettreAJourObjet();
    }

    else {
        mettreAJourObjet();
    }

    
    
}

function verifieNouveauNom(onglet){
    // Récupérer le nouveau nom d'onglet
    let nouveauNom = onglet.innerText.trim();
        
    // Vérifier si le nouveau nom est différent de l'ancien nom
    if (nouveauNom !== ancienNomOngletEnCoursDedition) {
        // Vérifier si le nouveau nom existe déjà dans les sauvegardes d'onglets
        if (!sauvegardesOnglets.hasOwnProperty(nouveauNom)) {
            // Mettre à jour le nom de l'onglet
            miseAJourNomOnglet(ancienNomOngletEnCoursDedition, nouveauNom);
        } else {
            alert("Le nouveau nom d'onglet spécifié existe déjà.");
            onglet.innerText=ancienNomOngletEnCoursDedition;
        }
    }
}

function miseAJourNomOnglet(ancienNom, nouveauNom) {
    // Vérifier si l'ancien nom d'onglet existe dans l'objet
    if (sauvegardesOnglets.hasOwnProperty(ancienNom)) {
        // Créer un nouvel objet temporaire
        let nouvelObjet = {};
        // Transférer les propriétés vers le nouvel objet avec le nom mis à jour
        for (let key in sauvegardesOnglets) {
            if (key === ancienNom) {
                nouvelObjet[nouveauNom] = sauvegardesOnglets[key];
            } else {
                nouvelObjet[key] = sauvegardesOnglets[key];
            }
        }
        // Remplacer l'ancien objet par le nouvel objet dans l'objet parent
        sauvegardesOnglets = nouvelObjet;
        console.log("Mise à jour nouveau nom "+nouveauNom);
        sauveObjet('classeur',sauvegardesOnglets,);
        sauveString('onglet-en-cours',ongletEnCours.innerText);
    } else {
        console.log("L'onglet avec l'ancien nom spécifié n'existe pas.");
    }
}


function changeOnglet(onglet){
    if (ongletEnCours){
        ongletEnCours.classList.remove('encours');
    }
    onglet.classList.add('encours');
    ongletEnCours = onglet;
    restaureObjet(); 
    sauveString('onglet-en-cours',ongletEnCours.innerText);   
}


function supprimeOnglet(onglet){
   let texteOnglet = onglet.innerText;
   const index = listeOnglets.indexOf(onglet); // Trouver l'index de l'onglet
   if (index !== -1) { // Vérifier si l'onglet existe dans le tableau
       listeOnglets.splice(index, 1); // Supprimer l'onglet à l'index trouvé
       console.log("Onglet supprimé :", onglet);
   } else {
       console.log("L'onglet spécifié n'existe pas dans la liste.");
   }
   onglet.remove();
   nombreOnglets-=1;
   // Supprimer la sauvegarde correspondante dans sauvegardesOnglets
   delete sauvegardesOnglets[texteOnglet];
   
   if (index>0){
    changeOnglet(listeOnglets[index-1]);
   } else {
    changeOnglet(listeOnglets[1]);
   }
}

function renommeOnglet(onglet){
    ancienNomOngletEnCoursDedition=onglet.innerText;
    onglet.contentEditable = true;
    onglet.focus();
    // Sélectionne tout le texte de l'onglet
    var selection = window.getSelection();
    var range = document.createRange();
    range.selectNodeContents(onglet);
    selection.removeAllRanges();
    selection.addRange(range);  

}

function menuContextuel(e,span) {
    if (!menuOuvert){
        menuOuvert=true;
        // Création du menu contextuel
        var menu = document.createElement('div');
        menu.classList.add('menu-contextuel');

        // Création des options du menu
        var optionRenommer = document.createElement('div');
        optionRenommer.textContent = "Renommer l'onglet";
        var optionDupliquer = document.createElement('div');
        optionDupliquer.textContent = "Dupliquer l'onglet";
        var optionSupprimer = document.createElement('div');
        optionSupprimer.textContent = "Supprimer l'onglet";

        // Ajout des écouteurs d'événements pour les options du menu
        optionRenommer.addEventListener('click', function() {
            renommeOnglet(span);
            menu.remove();
        });

        optionDupliquer.addEventListener('click', function() {
            nouvelOnglet('dupliquer',span);
            menu.remove();
            menuOuvert=false;
        });

        optionSupprimer.addEventListener('click', function() {
            supprimeOnglet(span);
            menu.remove();
            menuOuvert=false;
        });

        // Ajout des options au menu
        menu.appendChild(optionRenommer);
        menu.appendChild(optionDupliquer);
        if (nombreOnglets>1){menu.appendChild(optionSupprimer);}

        // Positionnement du menu contextuel
        menu.style.left = e.clientX + 'px';
        menu.style.top = e.clientY + 'px';

        // Ajout du menu au corps du document
        document.body.appendChild(menu);

        // Fermeture du menu contextuel lors du clic en dehors de celui-ci
        document.addEventListener('click', function(e) {
            if (!menu.contains(e.target)) {
                menu.remove();
                menuOuvert=false;
            }
        });
    }
}

function raz(){
    let confirmation = confirm("Voulez-vous vraiment tout effacer ?");
    if (confirmation){
        localStorage.clear();
        let TousLesSpanOnglets = divOnglets.querySelectorAll('.onglet');
        TousLesSpanOnglets.forEach(onglet => {
            onglet.remove();
        });
        valeursDepart();
        autoDeplacer('auto','raz');
        inputsEffectifs.forEach (input => {
            input.value=0;
        });
        divClasses.innerHTML='';
        init();
        nouvelOnglet('nouveau');
        traiter('auto');
    }
}

function autoDeplacer(valeur,provenance){
    let boutonsDeplacer = document.querySelectorAll('.modif, .plusmoins, .bouton-plus-classe');
    let reglagesAuto=document.querySelectorAll('.reglage-auto');
    let inputs = document.getElementsByTagName('input');

    if (provenance==='raz' || valeur && !auto){
        let confirmation=null;
        if (modif){
            confirmation = confirm("La répartition automatique va effacer vos répartitions manuelles.\nÊtes-vous sûr ?");
        }
        if (!modif || confirmation) {
            modif=false;
            body.style.background=null;
            auto=true;
            boutonDeplacer.classList.remove('actif');
            boutonAuto.classList.add('actif');
            // divEffectifs.classList.remove('grise');
            boutonsDeplacer.forEach(bouton => {
                bouton.classList.add('hide');       
            });
            /*
            reglagesAuto.forEach(bouton => {
                bouton.classList.remove('grise');       
            });
            for (var i = 0; i < inputs.length; i++) {
                inputs[i].disabled = false;
            }
            */
            traiter();
        } else {
            auto=false;
        }

    } else if (provenance==='depuisStockage' || auto && !valeur) {
        auto=false;
        body.style.backgroundColor='rgb(187, 218, 251)';
        boutonDeplacer.classList.add('actif');
        boutonAuto.classList.remove('actif');
        //divEffectifs.classList.add('grise');
  
        boutonsDeplacer.forEach(bouton => {
            bouton.classList.remove('hide');       
        });
              /*
        reglagesAuto.forEach(bouton => {
            bouton.classList.add('grise');       
        });
        for (var i = 0; i < inputs.length; i++) {
            inputs[i].disabled = true;
        }
        */
    }
    mettreAJourObjet();
}

function deplace(indiceClasse,indiceNiveau,sens){

    modif=true;
    
    classeSource=repartition[`classe_${indiceClasse}`];
    classeCible=repartition[`classe_${indiceClasse+sens}`];

    if(classeSource.effectifsParNiveaux[indiceNiveau] >0){
        classeSource.effectifsParNiveaux[indiceNiveau]-=1;
        classeCible.effectifsParNiveaux[indiceNiveau]+=1;
        classeSource.effectif_total-=1;
        classeCible.effectif_total+=1;

        valeursClasse[indiceClasse].innerText = classeSource.effectif_total + ' élèves';
        valeursClasse[indiceClasse+sens].innerText = classeCible.effectif_total + ' élèves';


        let valeursNiveauxSource = Array.from(divsClasses[indiceClasse].querySelectorAll('.valeur-zone-valeurs'));        // Répartition globale
        let valeursNiveauxCible = Array.from(divsClasses[indiceClasse+sens].querySelectorAll('.valeur-zone-valeurs'));        // Répartition globale

        valeursNiveauxSource[indiceNiveau].innerText=classeSource.effectifsParNiveaux[indiceNiveau]; // On met à jour l'affichage
        valeursNiveauxCible[indiceNiveau].innerText=classeCible.effectifsParNiveaux[indiceNiveau]; // On met à jour l'affichage


        valeursNiveauxCible[indiceNiveau].parentNode.classList.remove('hide');
        if (classeSource.effectifsParNiveaux[indiceNiveau]===0){valeursNiveauxSource[indiceNiveau].parentNode.classList.add('hide');}

        if (classeCible.effectifsParNiveaux[indiceNiveau]>0) {
            let boutonMoins = valeursNiveauxCible[indiceNiveau].parentNode.querySelector('.moins');
            boutonMoins.classList.remove('bouton-fermer-niveau');
        }

        sauveObjet('classeur',sauvegardesOnglets);
    }

}


function ajouteNiveau(indiceClasse) {

    divAjoutNiveaux.classList.remove('hide');
    darkbox.classList.remove('hide');

    classeSource = repartition[`classe_${indiceClasse}`];
    indiceClasseSource = indiceClasse;
    console.log(classeSource.effectifsParNiveaux);
    let tour=0;
    boutonsAjoutNiveau.forEach(bouton => {
        console.log(classeSource.effectifsParNiveaux[tour]);
        if (classeSource.effectifsParNiveaux[tour]===0){
            bouton.disabled=false;
        } else {
            bouton.disabled=true;
        }
        bouton.destination=classeSource;
        tour+=1;
    });


}

function ajouteNiveauSpecifie(indice) {
    let divNiveaux = Array.from(divsClasses[indiceClasseSource].querySelectorAll('.detail'));
    divNiveaux[indice].classList.remove('hide');
    let boutonMoins = divNiveaux[indice].querySelector('.moins');
    boutonMoins.classList.add('bouton-fermer-niveau'); 
    sauveBool('modif',modif);
    ferme(divAjoutNiveaux);
}


function plusmoins(indiceClasse,indiceNiveau,sens){

    modif=true;
    
    classeSource=repartition[`classe_${indiceClasse}`];
    let valeursNiveauxSource = Array.from(divsClasses[indiceClasse].querySelectorAll('.valeur-zone-valeurs'));        // Répartition globale

    if (sens===1 || classeSource.effectifsParNiveaux[indiceNiveau] > 0){

        classeSource.effectifsParNiveaux[indiceNiveau]+=sens;
        classeSource.effectif_total+=sens;
        effectifTotal+=sens;
        moyenneParClasse=effectifTotal/nombreDeClasses;

        let niveauKey = Object.keys(niveaux)[indiceNiveau];
        let niveauObjet = niveaux[niveauKey];

        niveauObjet.effectif += sens;
        document.getElementById('input-' + niveauKey).value = niveauObjet.effectif;

        valeurEffectifTotal.innerText=effectifTotal;
        valeurMoyenne.innerText = moyenneParClasse.toFixed(1).replace(".", ",");


        valeursClasse[indiceClasse].innerText = classeSource.effectif_total + ' élèves';


        valeursNiveauxSource[indiceNiveau].innerText=classeSource.effectifsParNiveaux[indiceNiveau]; // On met à jour l'affichage

        if (classeSource.effectifsParNiveaux[indiceNiveau]===0){valeursNiveauxSource[indiceNiveau].parentNode.classList.add('hide');}
        else {
            let boutonMoins = valeursNiveauxSource[indiceNiveau].parentNode.querySelector('.moins');
            boutonMoins.classList.remove('bouton-fermer-niveau');
        }
        mettreAJourObjet();
    } else {
        valeursNiveauxSource[indiceNiveau].parentNode.classList.add('hide');
    }
}



// Répartition des niveaux dans les classes
function repartitionClasses(mode){
    console.log('----------------- DÉBUT DE LA RÉPARTITION ---------------');
    valeursClasse=divClasses.querySelectorAll('.valeur-classe'); // Liste des compteurs d'effectifs de chaque classe
    divsClasses=Array.from(divClasses.querySelectorAll('.classe')); // Liste des blocs classes
    let i=0; // permettra de savoir à quelle classe on en est

    let effectifEcoleNonIsoleResteAplacer;
    let nombreDeClassesNonIsoleesAremplir;
    if (mode==='auto'){
        effectifEcoleNonIsoleResteAplacer = nombreElevesNonIsoles;
        nombreDeClassesNonIsoleesAremplir = nombreDeClassesNonIsolees;
        console.log('élèves non isolés '+effectifEcoleNonIsoleResteAplacer);
        console.log('classes non isolées  '+nombreDeClassesNonIsoleesAremplir);
    }


    restesEffectifsNiveaux=[];    

    for (let niveau in niveaux) {
        // Copie de l'effectif du niveau en cours vers la liste des effectifs temporaires
        let resteEffectifNiveau = niveaux[niveau].effectif;
        restesEffectifsNiveaux.push(resteEffectifNiveau);
        niveaux[niveau].nombreDeDivisions=niveaux[niveau].isoler;
    };

    for (let classe in repartition){
        if (mode==='auto'){
            // Nombre d'élèves de TPS PS MS CP ... dans cette classe, au début on met tout à zéro.        
            repartition[classe].effectifsParNiveaux = [0,0,0,0,0,0,0,0,0,];
            repartition[classe].nombreDeNiveaux = 0;
            repartition[classe].effectif_total = 0;
        }
    }


    for (let classe in repartition) { // Lancement de la boucle pour chaque classe

        console.log("---------- CLASSE "+(i+1));
        valeursNiveaux = Array.from(divsClasses[i].querySelectorAll('.valeur-zone-valeurs'));        // Répartition globale
        valeursNiveaux.forEach(valeurNiveau =>{
            valeurNiveau.innerText="0";
        });

        j=0; // Permettra de connaître le niveau à traiter, on commence à zéro.
        ok=true; // Variable qui permet de traiter ou pas un niveau.
        let classeIsolee=false;
            
        places = placesRestantes = Math.round((effectifEcoleNonIsoleResteAplacer - correctifEleves) / (nombreDeClassesNonIsoleesAremplir - correctifClasses));
        console.log('eleves à répartir '+(effectifEcoleNonIsoleResteAplacer - correctifEleves))
        console.log('classes à répartir '+(nombreDeClassesNonIsoleesAremplir - correctifClasses))
        console.log('places théoriques '+places)

        let placesDejaAttribuees = 0;

        let limite=50;

        // Répartition par niveau
        restesEffectifsNiveaux.forEach(resteEffectif => { // Traitement d'un niveau (ex. PS)
            if (ok) {
                if (mode==='auto'){

                    niveau = listeNiveaux[j];
                    console.log(listeNiveaux[j]);
                    console.log('Places déja attribuées dans cette classe '+placesDejaAttribuees);
                    console.log('placesRestantes dans cette classe'+placesRestantes);
                    console.log('Reste à placer de ce niveau '+resteEffectif)

                    if (resteEffectif>0 && niveaux[niveau].isoler){ // Si le niveau est marqué "isoler" ...
                        classeIsolee=true;
                        console.log('Classe isolée');
                    } else {
                        classeIsolee=false;
                        console.log('Classe non isolée');
                    }
                    
                    if (resteEffectif>0 && niveaux[niveau].max < limite){
                        limite = niveaux[niveau].max;
                        console.log('limite '+limite);
                    }

                    // Par défaut on attribue autant de places qu'il en reste de disponible, dans la limite de l'effectif non placé du niveau.
                    placesAattribuer = Math.min(resteEffectif,placesRestantes);
                    console.log('Places à attribuer (initial) '+placesAattribuer)

                    //------------------- Ajustements ----------------------//                                     
                    
                    // Si le nombre de places disponibles est inférieur à l'effectif minimal souhaité pour un groupe ...          
                    if(resteEffectif > placesRestantes && placesAattribuer > 0 &&  placesAattribuer < minGroupe){
                        if (prioriteInf){placesAattribuer = Math.min(minGroupe,resteEffectif);} // ... on outrepasse et on prend le nombre minimum pour un groupe, dans la limite de ce que peut fournir le niveau...
                        else {placesAattribuer = 0;console.log('Places attribuées '+placesAattribuer);} // ... ou bien on ne prend personne et on reporte à la classe suivante.     
                    }
                    //---------------------------------------------------------------------//

                    // Si après partage le nombre d'élèves qu'on laisse à la classe suivante est inférieur à l'effectif minimal souhaité pour un groupe ...
                    if (resteEffectif > 0 && resteEffectif - placesAattribuer != 0 && resteEffectif - placesAattribuer < minGroupe){
                        console.log('resteEffectif '+resteEffectif+' placesRestantes '+placesRestantes)
                        if (resteEffectif - placesRestantes <= 2 ){placesAattribuer = resteEffectif;} // ... on garde tout le monde dans la classe courante ...
                        else { // ... ou alors ...
                            if(resteEffectif >= 2*minGroupe){ // si le nombre d'élèves à placer est >= au double du minimum groupe,
                                placesAattribuer = resteEffectif - minGroupe; // on place tous les élèves du niveau moins le minimum groupe qu'on laisse pour la classe suivante.
                                console.log('Places attribuées '+placesAattribuer);
                            } else {
                                placesAattribuer = 0; // sinon on ne place personne et tous le groupe ira dans la classe suivante.
                                console.log('Places attribuées '+placesAattribuer);
                            }
                            if (!caseSauter.checked){console.log('passage à la classe suivante');ok=false;} // si on n'a pas coché "sauter" on arrête de traiter des niveaux.
                        } 
                    }
                    //---------------------------------------------------------------------//

                    // Si le niveau est marqué groupé ...
                    if (grouperEffectifsNiveaux[j]){
                        // On place tous les élèves, quitte à dépasser la moyenne cible, mais dans une certaine limite
                        if (placesDejaAttribuees + resteEffectif <= places * 1.2){
                            console.log('groupement en dépassement '+resteEffectif)
                            placesAattribuer = resteEffectif;
                            console.log('Places attribuées '+placesAattribuer);
                        }
                        else {
                        // Si trop d'élèves, on ne place personne, ce qui reporte le groupe à la classe suivante.
                            console.log('groupement impossible trop d élèves : '+resteEffectif)
                            placesAattribuer = 0;
                            console.log('Places attribuées '+placesAattribuer);
                            if (!caseSauter.checked){console.log('passage à la classe suivante');ok=false;}  // si on n'a pas coché "sauter" on arrête de traiter des niveaux.
                        }
                    }
                    //---------------------------------------------------------------------//

                    // Si le niveau est marqué isolé ...
                    if (isolerEffectifsNiveaux[j]){
                        if (placesDejaAttribuees===0){ // Si on n'a encore placé personne dans cette classe ...
                            if (resteEffectif <= limite){placesAattribuer = resteEffectif;
                                console.log('Places attribuées '+placesAattribuer);} // Dans la limite fixée, on place tout le monde.
                            else {
                                placesAattribuer = Math.ceil(resteEffectif/niveaux[niveau].nombreDeDivisions);
                                niveaux[niveau].nombreDeDivisions-=1;
                                console.log('Places attribuées '+placesAattribuer); } // Sinon on scinde.
                        } else {
                            placesAattribuer = 0; // Si on avait déjà placé des élèves dans cette classe, on ne place personne.
                            console.log('Places attribuées '+placesAattribuer);
                        }
                    }
                    //---------------------------------------------------------------------//

                    // Anticipation du niveau suivant s'il est marqué groupé
                    if (grouperEffectifsNiveaux[j+1]){
                        // Calcul des élèves du niveau courant qui seront reportés sur la classe suivante
                        let debordemementClasseSuivante = resteEffectif - placesAattribuer;                        
                        if (debordemementClasseSuivante > 0){
                            // Si c'est le cas on calcule combien seront reportés sur la classe d'après.
                            let debordemementClasseSuivanteSuivante = debordemementClasseSuivante + restesEffectifsNiveaux[j+1] - niveaux[listeNiveaux[j+1]].max;
                            // Si ce débordement est inférieur au "Minimum Groupe", on ajoute ces élèves à la classe courante. 
                            if (debordemementClasseSuivanteSuivante > 0 && debordemementClasseSuivanteSuivante < minGroupe){
                                placesAattribuer += debordemementClasseSuivanteSuivante;
                                console.log('Places attribuées '+placesAattribuer);
                            }
                        }
                    }
                    //---------------------------------------------------------------------//

                    // Limite
                    if (limite && placesDejaAttribuees + placesAattribuer > limite){
                        placesAattribuer = limite - placesDejaAttribuees;
                        console.log('Places attribuées '+placesAattribuer);

                        let resteEffectifApresAttribution = resteEffectif - placesAattribuer;
                        let debordementFinal = resteEffectifApresAttribution % limite;
                        let differentiel = minGroupe - debordementFinal;
                        console.log('differentiel' +differentiel)
                        if (differentiel > 0){
                            let nombreDeClassesQuiVontSePartagerLeDifferentiel = Math.floor(resteEffectif / limite);
                            console.log('nombreDeClassesQuiVontSePartagerLeDifferentiel '+nombreDeClassesQuiVontSePartagerLeDifferentiel)
                            placesAattribuer-= Math.floor(differentiel / nombreDeClassesQuiVontSePartagerLeDifferentiel);
                            console.log('Places attribuées '+placesAattribuer);
                        }



                        if (placesAattribuer<minGroupe){
                            placesAattribuer=0;
                            
                        }
                    }
                    //---------------------------------------------------------------------//

                    // Dernière classe
                    if (i===nombreDeClasses - 1){
                        placesAattribuer = resteEffectif; // On place tous les élèves coûte que coûte.
                        console.log('Places attribuées '+placesAattribuer);
                    }
                    //---------------------------------------------------------------------//
            }
                // Application de la répartition
                if (mode==='manuel' || placesAattribuer > 0){ // (si des places sont à attribuer)
                    if (mode!='manuel'){
                        repartition[classe].nombreDeNiveaux+=1;
                        repartition[classe].effectifsParNiveaux[j]+=placesAattribuer; // On met à jour l'effectif courant de la classe
                        placesRestantes-=placesAattribuer; // On soustrait les élèves places aux places restantes
                        placesDejaAttribuees+=placesAattribuer;
                        restesEffectifsNiveaux[j]-=placesAattribuer;// On les soustrait aussi à l'effectif à placer au niveau en cours               
                        console.log('restesEffectifsNiveaux niveau ['+j+'] '+restesEffectifsNiveaux[j])
                        console.log('Soustraction de '+placesAattribuer)
                        console.log('restesEffectifsNiveaux niveau ['+j+'] '+restesEffectifsNiveaux[j])
                        if (classeIsolee){ // Si le niveau est marqué "isoler" ...
                            console.log('passage à la classe suivante');
                            ok=false;
                        }
                    }

                    valeursNiveaux[j].innerText=repartition[classe].effectifsParNiveaux[j]; // On met à jour l'affichage
                    console.log('Mise à jour affichage valeur '+listeNiveaux[j]+' '+repartition[classe].effectifsParNiveaux[j])
                    


                // S'il y avait déjà des élèves placés, mais qu'on n'a placé personne de nouveau, et que la case "sauter" n'est pas cochée, on ne place plus d'autres niveaux dans cette classe.
                } else if (placesDejaAttribuees>0 && placesAattribuer===0 && !caseSauter.checked){console.log('passage à la classe suivante');ok=false;}


                if (mode==='auto' && placesRestantes<=0){console.log('passage à la classe suivante');ok=false;} // Si plus de place dans cette classe, on ne traite plus d'autres niveaux.
                if (!caseTripleNiveaux.checked && repartition[classe].effectifsParNiveaux[1]===0 && repartition[classe].nombreDeNiveaux===2){
                    console.log('passage à la classe suivante');
                    ok=false;
                }
                if (i===nombreDeClasses - 1 || mode==='manuel'){ok=true;} // Si c'est la dernière classe, on traite quand même.
            }


            // On rend invisibles les effectifs nuls.
            if (valeursNiveaux[j].innerText==='0'){valeursNiveaux[j].parentNode.classList.add('hide');}
            else {valeursNiveaux[j].parentNode.classList.remove('hide');}
            // On avance d'un niveau avant de relancer la boucle.
            j+=1;
        });

        // Calcul de l'effectif total de la classe
        let somme=0;
        for (let k = 0; k < repartition[classe].effectifsParNiveaux.length; k++) {
            somme += repartition[classe].effectifsParNiveaux[k];
        }        
        repartition[classe].effectif_total = somme;

        if (!classeIsolee) {
            effectifEcoleNonIsoleResteAplacer-= somme;
            nombreDeClassesNonIsoleesAremplir-=1;
            console.log('effectifEcoleNonIsoleResteAplacer '+effectifEcoleNonIsoleResteAplacer)                        
        }
        
        // Mise à jour de l'affichage
        
        valeursClasse[i].innerText = repartition[classe].effectif_total + ' élèves';
        
        /*
        if (mode==='auto' && !classeIsolee && repartition[classe].effectif_total < moyenneEffectifHorsIsoler && limite < moyenneEffectifHorsIsoler){
            nombreDelevesAcompterEnMoins+= repartition[classe].effectif_total;
            classesAcompterEnMoins+=1;
        }
        */
        i+=1; // On avance d'une classe avant de relancer la boucle
    
    }

    mettreAJourObjet();


    //sauveObjet('niveaux',niveaux);
    //sauveObjet('repartition',repartition);
    //sauveInt('nombreDeClasses',nombreDeClasses);
    //sauveInt('minGroupe',minGroupe);
    //sauveBool('modif',modif);
    //sauveBool('auto',auto);
    //sauveBool('caseSauter.checked',caseSauter.checked);
    //sauveBool('caseAuto.checked',caseAuto.checked);
    //sauveBool('caseTripleNiveaux.checked',caseTripleNiveaux.checked);

    correctifEleves=nombreDelevesAcompterEnMoins;
    correctifClasses=classesAcompterEnMoins;

    

    tour+=1;
    if (mode==='auto' && tour<2){
      repartitionClasses('auto');
    }
    
}

// Fonction pour cloner les objets en profondeur
function clonerObjetEnProfondeur(objet) {
    if (typeof objet !== 'object' || objet === null) {
        return objet; // Retourne directement les types non-objets ou les valeurs null
    }

    let clone = Array.isArray(objet) ? [] : {}; // Crée un nouveau tableau ou objet

    for (let cle in objet) {
        if (Object.prototype.hasOwnProperty.call(objet, cle)) {
            clone[cle] = clonerObjetEnProfondeur(objet[cle]); // Récursion pour chaque propriété
        }
    }

    return clone;
}

function mettreAJourObjet() {
    // Récupérer le texte de l'élément ongletEnCours
    let ongletEnCoursTexte = ongletEnCours.innerText;

    // Vérifier si l'entrée dans sauvegardesOnglets existe déjà
    if (!sauvegardesOnglets[ongletEnCoursTexte]) {
        sauvegardesOnglets[ongletEnCoursTexte] = {}; // Créer un nouvel objet s'il n'existe pas
    }

    // Créer une copie distincte de l'objet de sauvegarde en fonction des valeurs actuelles des variables globales
    let nouvelObjetDeSauvegarde = clonerObjetEnProfondeur(sauvegardesOnglets[ongletEnCoursTexte]);

    // Mettre à jour l'objet de sauvegarde avec les valeurs actuelles des variables globales
    nouvelObjetDeSauvegarde.auto = auto;
    nouvelObjetDeSauvegarde.caseSauter = caseSauter.checked;
    nouvelObjetDeSauvegarde.caseTripleNiveaux = caseTripleNiveaux.checked;
    nouvelObjetDeSauvegarde.minGroupe = minGroupe;
    nouvelObjetDeSauvegarde.modif = modif;
    nouvelObjetDeSauvegarde.nombreDeClasses = nombreDeClasses;
    nouvelObjetDeSauvegarde.niveaux = clonerObjetEnProfondeur(niveaux); // Copie de niveaux
    nouvelObjetDeSauvegarde.repartition = clonerObjetEnProfondeur(repartition); // Copie de repartition
    for (sauvegarde in sauvegardesOnglets){
     //   console.log('TPS effectif '+sauvegardesOnglets[sauvegarde].niveaux.tps.effectif)
    }

    // Mettre à jour l'objet de sauvegarde dans sauvegardesOnglets
    sauvegardesOnglets[ongletEnCoursTexte] = nouvelObjetDeSauvegarde;

    sauveObjet('classeur',sauvegardesOnglets);
}

function restaureObjet(mode) {
    // Créer une copie distincte de modeleSauvegarde
    let modeleSauvegardeCopie = JSON.parse(JSON.stringify(modeleSauvegarde));

    // Récupérer le texte de l'élément ongletEnCours
    let ongletEnCoursTexte = ongletEnCours.innerText;

    // Récupérer l'objet correspondant à ongletEnCoursTexte
    let objetAReconstituer;
    if (mode === 'nouveau') {
        objetAReconstituer = modeleSauvegardeCopie;
    } else {    
        objetAReconstituer = sauvegardesOnglets[ongletEnCoursTexte];
    }

    // Restaurer les variables globales à partir de l'objet s'il existe
    if (objetAReconstituer) {
        auto = objetAReconstituer.auto;
        caseSauter.checked = objetAReconstituer.caseSauter;
        caseTripleNiveaux.checked = objetAReconstituer.caseTripleNiveaux;
        minGroupe = objetAReconstituer.minGroupe;
        modif = objetAReconstituer.modif;
        nombreDeClasses = objetAReconstituer.nombreDeClasses;
        niveaux = { ...objetAReconstituer.niveaux }; // Copie de niveaux
        repartition = { ...objetAReconstituer.repartition }; // Copie de repartition
    }
    majAffichages(); // Assurez-vous d'appeler majAffichages() ici si elle est nécessaire
}


function telechargerCsv() {
    creeTableau();
    // Convertir le tableau à deux niveaux en une chaîne CSV
    let csv = tableau.map(row => row.join(';')).join('\n');
    
    // Créer un objet Blob
    let blob = new Blob([csv], { type: 'text/csv' });
    
    // Créer un objet URL à partir du Blob
    let url = window.URL.createObjectURL(blob);
    
    // Créer un élément d'ancre pour télécharger le fichier
    let a = document.createElement('a');
    a.href = url;
    
    // Définir le nom du fichier
    a.download = 'repartition.csv';
    
    // Simuler un clic sur l'ancre pour déclencher le téléchargement
    a.click();
    
    // Libérer l'objet URL
    window.URL.revokeObjectURL(url);
}

function creeTableau(){

    tableau = [
        ['classe','TPS','PS','MS','GS','CP','CE1','CE2','CM1','CM2','total']
    ];

    for (classe in repartition){
        let nouvelleClasse = [];
        nouvelleClasse.push(repartition[classe].nom);
        j=0;
        for (effectif in repartition[classe].effectifsParNiveaux){
            let nouvelEffectif = repartition[classe].effectifsParNiveaux[j];
            nouvelleClasse.push(nouvelEffectif);
            j+=1;
        }
        let nouvelEffectifTotal = repartition[classe].effectif_total;
        nouvelleClasse.push(nouvelEffectifTotal);
        tableau.push(nouvelleClasse);
    }

    let ligneFinale = [];
    ligneFinale.push('total');

    for (niveau in niveaux){
        let nouvelEffectif = niveaux[niveau].effectif;
        ligneFinale.push(nouvelEffectif);
        }
    ligneFinale.push(effectifTotal);    
    tableau.push(ligneFinale);
}


function sauveInt(cle,valeur){
    localStorage.setItem('repartition-'+cle, valeur);
}

function sauveObjet(cle,valeur){
    let valeurAsauver = JSON.stringify(valeur);
    localStorage.setItem('repartition-'+cle, valeurAsauver);
}

function sauveBool(cle,valeur){    
    localStorage.setItem('repartition-'+cle, valeur.toString());
}

function sauveString(cle,valeur){    
    localStorage.setItem('repartition-'+cle,valeur);
}

function litInt(cle){
    return parseInt(localStorage.getItem('repartition-'+cle));
}

function litString(cle){
    return localStorage.getItem('repartition-'+cle);
}

function litObjet(cle){  
    if (cle==="niveaux"){
        console.log(JSON.parse(localStorage.getItem('repartition-'+cle)));
    }  
    return JSON.parse(localStorage.getItem('repartition-'+cle));

}

function litBool(cle){   
    let valeurBoleenneTexte =  localStorage.getItem('repartition-'+cle);
    return (valeurBoleenneTexte === 'true');
}


// Vérifie si l'URL contient "/dev"
if (window.location.href.includes("/dev")) {
    // Sélectionne le corps de la page
    const body = document.querySelector('body');

    // Crée un élément pour le message
    const message = document.createElement('div');
    message.textContent = 'M.A.J. 05/04/2024 VERSION POUR TESTS UNIQUEMENT - NE PAS PUBLIER';

    // Ajoute une classe au message pour le style CSS si nécessaire
    message.classList.add('dev-message'); // Vous pouvez personnaliser cette classe selon vos besoins

    // Ajoute le message en tant que premier enfant du corps de la page
    body.insertBefore(message, body.firstChild);
}

function modifEffectif(event){
    if (auto){traiter()}
    else {
        alert('Vous êtes en mode manuel.\nPour modifier les effectifs, passez en mode "Répartition automatique".')
        let inputId = event.target.id;
        let ancienneValeur = niveaux[inputId.substring(6)].effectif;
        document.getElementById(inputId).value=ancienneValeur;
    }
}

function modifValeur(){
    if (auto){traiter();}
}

function ouvre(div){
    div.classList.remove('hide');
    darkbox.classList.remove('hide');
}


function ferme(div){
    div.classList.add('hide');
    darkbox.classList.add('hide');
}


function enregistreEtTelecharge() {
    // Initialiser un objet pour stocker toutes les clés et leurs valeurs
    var allData = {};

    // Parcourir toutes les clés du stockage local
    for (var i = 0; i < localStorage.length; i++) {
        var key = localStorage.key(i);
        var value = localStorage.getItem(key);
        // Stocker la clé et sa valeur dans l'objet
        allData[key] = value;
    }

    // Convertir les données en format JSON
    var jsonData = JSON.stringify(allData);

    // Créer un objet Blob à partir des données JSON
    var blob = new Blob([jsonData], { type: 'application/json' });

    // Obtenir la date du jour
    var today = new Date();
    var dateString = today.getFullYear() + "-" + (today.getMonth() + 1) + "-" + today.getDate();

    // Nom du fichier avec la date du jour
    var filename = "repartition_" + dateString + ".json";

    // Créer une URL de données à partir du Blob
    var url = URL.createObjectURL(blob);

    // Créer un élément <a> pour le téléchargement
    var downloadLink = document.createElement("a");
    downloadLink.href = url;
    downloadLink.download = filename;
    downloadLink.textContent = "Télécharger le fichier";

    // Ajouter le lien de téléchargement à la page
    document.body.appendChild(downloadLink);

    // Supprimer le lien de téléchargement après un court délai pour éviter les fuites de mémoire
    setTimeout(function() {
        document.body.removeChild(downloadLink);
        // Nettoyer l'URL de données
        URL.revokeObjectURL(url);
    }, 100);

    // Cliquez sur le lien pour déclencher la boîte de dialogue de téléchargement
    downloadLink.click();

    console.log("Toutes les clés du stockage local ont été sauvegardées et téléchargées.");
}

function ouvrirFichier() {
    var reponse = confirm("Voulez-vous d'abord enregistrer le classeur actuel ?");
    if (reponse) {
        // L'utilisateur a choisi d'enregistrer le classeur actuel, appelez la fonction pour cela
        enregistreEtTelecharge();   }
    // Ouvrir la boîte de dialogue de fichiers
    document.getElementById('fileInput').click();
}

function ouvrir(file) {
    var reader = new FileReader();

    reader.onload = function(event) {
        var jsonData = event.target.result;
        var parsedData = JSON.parse(jsonData);

        for (var key in parsedData) {
            localStorage.setItem(key, parsedData[key]);
        }

        console.log("Données du fichier chargées dans le stockage local.");
        // Assurez-vous que la fonction lireStockageLocal est définie ailleurs dans votre code
        lireStockageLocal();
    };

    reader.readAsText(file);
}



    function replier(){
        divEffectifs.classList.toggle('replie');
        boutonReplier.classList.toggle('retourne');

        // Forcer le recalcul du style pour déclencher la transition
        setTimeout(() => {
            divEffectifs.classList.toggle('force-transition');
        }, 10); // un délai très court pour garantir que la transition démarre immédiatement
    
}