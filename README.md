# Répartition

Répartition est un outil de visualisation et d’organisation pour les répartitions de classes à l’école primaire.

Cette application ne se substitue pas à une réflexion d’équipe, ni à la prise en compte des particularités locales, profils d’élèves, projets pédagogiques…

Elle vous propose une première ventilation automatique, qui peut ne pas correspondre à une réalité envisageable, mais vous permet d’avoir une base, à partir de laquelle vous n'avez plus qu’à déplacer des élèves.

Vous récupérez ensuite votre répartition au format tableur (CSV).

## Comment faire ?

- Renseignez vos effectifs dans les encadrés correspondants.
- Ajustez si nécessaire les paramètres.
- Observez la répartition proposée, et réajustez éventuellement vos paramètres.
- Cliquez sur “Déplacer des élèves”.
- Faites les ajustements nécessaires au moyen des flèches marron (en faisant “monter” ou “descendre” des élèves).
- Téléchargez vos effectifs en CSV (format utilisable avec un tableur).


## Limites
Répartition est un projet d’application qui a son intérêt et ses limites.

Il est assez complexe voire impossible de faire fonctionner un algorithme idéal qui calculerait la meilleure répartition possible en tenant compte de toutes les spécificités. Chaque école est différente, et l’application ne lit pas dans les pensées des collègues.

Et l’informatique ne fait pas de miracles. Elle ne pourra par exemple pas produire des classes à simple niveau ET des effectifs strictement équilibrés.

La répartition proposée a essentiellement pour but de fournir un tableau de base. Elle fonctionne ensuite comme un tableur amélioré, car vous pouvez déplacer facilement des élèves d’une ligne à l’autre.

## Données personnelles
L’application ne collecte aucune donnée. Elle s’exécute intégralement dans la mémoire locale de votre navigateur (Firefox, Chrome …), c’est ce dernier qui fait tout le travail. C’est pourquoi elle peut d’ailleurs fonctionner hors-ligne.

Elle n’utilise aucun cookie, aucun traceur, n’affiche aucune publicité.

Vos répartition est automatiquement sauvegardée dans la mémoire locale de votre navigateur (que vous pouvez effacer manuellement via la fonction “Supprimer l’historique récent” ou “Effacer les données de navigation”). Elle ne “remonte” pas vers le serveur.

## Licence
Répartition est une application libre et gratuite, sous licence GNU/GPL, écrite par Arnaud Champollion.

Elle peut être utilisée de façon illimitée sans avoir besoin de créer de compte, et peut aussi fonctionner hors-ligne.

## Proposer ou signaler

Répartition est un projet en évolution qui se nourrit de vos retours.

Si vous avez une suggestion d’amélioration à faire ou que vous constatez un dysfonctionnement, vous pouvez le signaler en rédigeant un ticket.




